# ROS 2 Neuron-Omnibot
## Installation
```
$ source /opt/ros/dashing/setup.bash
$ git clone https://bitbucket.org/ROScube/omnibot.repos.git ~/ros2_neuron_omnibot_ws
$ mkdir -p ~/ros2_neuron_omnibot_ws/src
$ cd ~/ros2_neuron_omnibot_ws
$ vcs import src < omnibot_https.repos
$ rosdep install --from-paths src --ignore-src -r -y
$ colcon build --symlink-install 
$ source ~/ros2_neuron_omnibot_ws/install/local_setup.bash
```



## Setup udev symbolic-link (execute at first time)

```
$ . ~/ros2_neuron_omnibot_ws/src/omnibot/omnibot/omnibot_base/startup/init_ydlidar.sh
$ . ~/ros2_neuron_omnibot_ws/src/omnibot/omnibot/omnibot_base/startup/init_neuron_tty.sh
```



## Launch base driver, ydlidar and EKF

```
$ source /opt/ros/dashing/setup.bash
$ source ~/ros2_neuron_omnibot_ws/install/local_setup.bash
$ ros2 launch omnibot_base omni_ekf.launch.py
```



## Launch Navigation 2 bringup

```
$ source /opt/ros/dashing/setup.bash
$ source ~/ros2_neuron_omnibot_ws/install/local_setup.bash
$ ros2 launch omnibot_bringup nav2_bringup_launch.py
```

